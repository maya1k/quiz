package database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import Quiz.*;

public class DBConnection {
	
	// getPassword, setPassword, addUser, deletUser
	// getPassword done
	protected static PreparedStatement setPasswordStmt; //done
	protected static PreparedStatement addUserStmt; //done
	protected static PreparedStatement deleteUserStmt; //done
	
	// sendMessage, readMessage, deleteMessage
	protected static PreparedStatement sendMessageStmt; //done
	protected static PreparedStatement readMessageStmt; //done
	protected static PreparedStatement deleteMessageStmt; //done
	
	// sendFriendRequest, acceptFriendRequest, declineFriendRequest
	protected static PreparedStatement sendFriendRequestStmt; //done
	protected static PreparedStatement acceptFriendRequestStmt1, acceptFriendRequestStmt2, acceptFriendRequestStmt3; 
	protected static PreparedStatement declineFriendRequestStmt; //done
	
	// sendChallenge, acceptChallenge, declineChallenge
	protected static PreparedStatement sendChallengeStmt; //done
	protected static PreparedStatement acceptChallengeStmt; //done
	protected static PreparedStatement declineChallengeStmt; //done
	protected static PreparedStatement completeChallengeStmt; //done
	
	// Quiz
	protected static PreparedStatement updateHighScoreStmt; //done
	protected static PreparedStatement updateHighScorerStmt;//done
	protected static PreparedStatement updateBestTimeStmt;//done
	protected static PreparedStatement updateBestTimeUserStmt; //done
	protected static PreparedStatement submitQuizStmt;

	// addQuestion, getQuestion
	// getQuestion
	protected static PreparedStatement storeQuestionStmt; //done
	protected static PreparedStatement addQuizQuestionLinkStmt; //done
	
	protected static PreparedStatement addAnnouncementStmt; //done
	protected static PreparedStatement deleteAnnouncementStmt;
	protected static PreparedStatement addAchievementStmt; //done
	protected static PreparedStatement addHistoryStmt; //done

	// Constructor
	public DBConnection() throws SQLException, IOException {
		 System.out.println("DBConnection starting ...");
		 System.out.println("Loading Driver Class");
		 
		 try{
			 Class.forName("com.mysql.jdbc.Driver");
		 } catch (ClassNotFoundException ex) {
			 System.out.println("FAILED: " + ex.toString());
			 throw new IllegalStateException(ex.toString());
		 }
		 
	}
	
	 public synchronized static void addAchievement (String achievement, int userid) throws IOException, SQLException {
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 addAchievementStmt = con.prepareStatement("insert into achievements values (null, ?, ?, ?)");

		 int i = 1;
		 addAchievementStmt.setString(i++, achievement);
		 addAchievementStmt.setInt(i++, userid);
		 java.sql.Date now = new java.sql.Date(System.currentTimeMillis());
		 addAchievementStmt.setDate(i++, now);
		 addAchievementStmt.executeUpdate();
		 
		 stmt.close();
		 addAchievementStmt.close();
		 con.close();
	 } 	

	 public synchronized static void addHistory (int userid, int score, int time, int quizid) throws IOException, SQLException {
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 addHistoryStmt = con.prepareStatement("insert into history values (null, ?, ?, ?, ?)");

		 int i = 1;
		 addHistoryStmt.setInt(i++, userid);
		 addHistoryStmt.setInt(i++, score);
		 addHistoryStmt.setInt(i++, time);
		 addHistoryStmt.setInt(i++, quizid);
		 addHistoryStmt.executeUpdate();
		 
		 stmt.close();
		 addHistoryStmt.close();
		 con.close();
	 } 		 
	 
	 public synchronized static void addAnnouncement (String message, int userid) throws IOException, SQLException {
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 addAnnouncementStmt = con.prepareStatement("insert into announcements values (null, ?, ?, ?)");

		 int i = 1;
		 addAnnouncementStmt.setString(i++, message);
		 addAnnouncementStmt.setInt(i++, userid);
		 java.sql.Date now = new java.sql.Date(System.currentTimeMillis());
		 addAnnouncementStmt.setDate(i++, now);
		 addAnnouncementStmt.executeUpdate();
		 
		 stmt.close();
		 addAnnouncementStmt.close();
		 con.close();
	 } 	
	 
	 public synchronized static void deleteAnnouncement (int announcementid) throws IOException, SQLException {
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 deleteAnnouncementStmt= con.prepareStatement("delete from announcements where announcementId = ?");
		 
		 int i = 1;
		 deleteAnnouncementStmt.setInt(i++, announcementid);
		 deleteAnnouncementStmt.executeUpdate();
		 
		 stmt.close();
		 con.close();
		 deleteAnnouncementStmt.close();
	 }	 
	 
	 public synchronized static void addUser (String username, String passwordhash, int hasmessage, int haschallenge, int hasfriendrequest, int isadmin) throws IOException, SQLException {
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 addUserStmt = con.prepareStatement("insert into users values (null, ?, ?, ?, ?, ?, ?)");

		 int i = 1;
		 addUserStmt.setString(i++, username);
		 addUserStmt.setString(i++, passwordhash);
		 addUserStmt.setInt(i++, hasmessage);
		 addUserStmt.setInt(i++, haschallenge);
		 addUserStmt.setInt(i++, hasfriendrequest);
		 addUserStmt.setInt(i++, isadmin);
		 addUserStmt.executeUpdate();
		 
		 stmt.close();
		 addUserStmt.close();
		 con.close();
	 }
	 
	 public synchronized static void setPassword (String username, String passwordhash) throws IOException, SQLException {	 
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 setPasswordStmt  = con.prepareStatement("update users SET passwordHash = ? where userName = ?");

		 int i = 1;
		 setPasswordStmt.setString(i++, passwordhash);
		 setPasswordStmt.setString(i++, username);
		 setPasswordStmt.executeUpdate();
		 
		 stmt.close();
		 con.close();
		 setPasswordStmt.close();
	 }
	 
	 
	 public synchronized static void deleteUser (String username) throws IOException, SQLException {
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 deleteUserStmt= con.prepareStatement("delete from users where userName = ?");
		 
		 int i = 1;
		 deleteUserStmt.setString(i++, username);
		 deleteUserStmt.executeUpdate();
		 
		 stmt.close();
		 con.close();
		 deleteUserStmt.close();
	 }
	 
	 public static String getPassword (String username) throws IOException, SQLException {
		 String passwordhash = null;
		 try {
			 Class.forName("com.mysql.jdbc.Driver");
			 java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
			 Statement stmt = con.createStatement();
			 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			 String sqlquery = "SELECT * FROM users WHERE userName = '"+ username + "'";
			 ResultSet rs = stmt.executeQuery(sqlquery);	
			 while (rs.next()){
			 	passwordhash = rs.getString("passwordHash");
			 }
			 con.close();
			 }catch (SQLException e) {
			 	e.printStackTrace();
			 } catch (ClassNotFoundException e) {
			 	e.printStackTrace();
			 }
		return passwordhash;	
	 }
	 
	 public synchronized static void sendMessage (int fromuserid, int touserid, String message) throws IOException, SQLException {
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 sendMessageStmt = con.prepareStatement("insert into messages values (null, ?, ?, ?, ?, ?, ?)");

		 int i = 1;
		 sendMessageStmt.setInt(i++, fromuserid);
		 sendMessageStmt.setInt(i++, touserid);
		 java.sql.Date now = new java.sql.Date(System.currentTimeMillis());
		 sendMessageStmt.setDate(i++, now);
		 sendMessageStmt.setString(i++, message);
		 sendMessageStmt.setInt(i++, 0);
		 sendMessageStmt.setInt(i++, 0);
		 sendMessageStmt.executeUpdate();
		 
		 stmt.close();
		 sendMessageStmt.close();
		 con.close();
	 }
	 
	 public synchronized static void sendFriendRequest (int fromuserid, int touserid) throws IOException, SQLException {
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 sendFriendRequestStmt = con.prepareStatement("insert into friendRequests values (null, ?, ?, ?, ?, ?)");

		 int i = 1;
		 sendFriendRequestStmt.setInt(i++, fromuserid);
		 sendFriendRequestStmt.setInt(i++, touserid);
		 java.sql.Date now = new java.sql.Date(System.currentTimeMillis());
		 sendFriendRequestStmt.setDate(i++, now);
		 sendFriendRequestStmt.setInt(i++, 0);
		 sendFriendRequestStmt.setInt(i++, 0);
		 sendFriendRequestStmt.executeUpdate();
		 
		 stmt.close();
		 sendFriendRequestStmt.close();
		 con.close();
	 }
	 
	 public synchronized static void sendChallenge (int fromuserid, int touserid, int quizid) throws IOException, SQLException {
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 sendChallengeStmt = con.prepareStatement("insert into challenges values (null, ?, ?, ?, ?, ?, ?, ?)");

		 int i = 1;
		 sendChallengeStmt.setInt(i++, fromuserid);
		 sendChallengeStmt.setInt(i++, touserid);
		 sendChallengeStmt.setInt(i++, quizid);
		 sendChallengeStmt.setInt(i++, 0);
		 sendChallengeStmt.setInt(i++, 0);
		 sendChallengeStmt.setInt(i++, 0);
		 java.sql.Date now = new java.sql.Date(System.currentTimeMillis());
		 sendChallengeStmt.setDate(i++, now);
		 sendChallengeStmt.executeUpdate();
		 
		 stmt.close();
		 sendChallengeStmt.close();
		 con.close();
	 } 
	 
	 public synchronized static void readMessage (int messageid) throws IOException, SQLException {	 
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 readMessageStmt  = con.prepareStatement("update messages SET isRead = 1 where messageId = ?");

		 int i = 1;
		 readMessageStmt.setInt(i++, messageid);
		 readMessageStmt.executeUpdate();
		 
		 stmt.close();
		 con.close();
		 readMessageStmt.close();
	 }
	 
	 public synchronized static void deleteMessage (int messageid) throws IOException, SQLException {	 
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 deleteMessageStmt  = con.prepareStatement("update messages SET isDeleted = 1 where messageId = ?");

		 int i = 1;
		 deleteMessageStmt.setInt(i++, messageid);
		 deleteMessageStmt.executeUpdate();
		 
		 stmt.close();
		 con.close();
		 deleteMessageStmt.close();
	 }
	 
	 public synchronized static void declineFriendRequest (int friendrequestid) throws IOException, SQLException {	 
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 declineFriendRequestStmt  = con.prepareStatement("update friendRequests SET isDeclined = 1 where friendRequestId = ?");

		 int i = 1;
		 declineFriendRequestStmt.setInt(i++, friendrequestid);
		 declineFriendRequestStmt.executeUpdate();
		 
		 stmt.close();
		 con.close();
		 declineFriendRequestStmt.close();
	 }	 

	 public synchronized static void declineChallenge (int challengeid) throws IOException, SQLException {	 
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 declineChallengeStmt  = con.prepareStatement("update challenges SET isDeclined = 1 where challengeId = ?");

		 int i = 1;
		 declineChallengeStmt.setInt(i++, challengeid);
		 declineChallengeStmt.executeUpdate();
		 
		 stmt.close();
		 con.close();
		 declineChallengeStmt.close();
	 }	 

	 public synchronized static void completeChallenge (int challengeid) throws IOException, SQLException {	 
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 completeChallengeStmt  = con.prepareStatement("update challenges SET completed = 1 where challengeId = ?");

		 int i = 1;
		 completeChallengeStmt.setInt(i++, challengeid);
		 completeChallengeStmt.executeUpdate();
		 
		 stmt.close();
		 con.close();
		 completeChallengeStmt.close();
	 }	 	 

	 public synchronized static void acceptChallenge (int challengeid) throws IOException, SQLException {	 
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 acceptChallengeStmt  = con.prepareStatement("update challenges SET isAccepted = 1 where challengeId = ?");

		 int i = 1;
		 acceptChallengeStmt.setInt(i++, challengeid);
		 acceptChallengeStmt.executeUpdate();
		 
		 stmt.close();
		 con.close();
		 acceptChallengeStmt.close();
	 }	 	 	 

	 public synchronized static String storeQuestion (String questiontype, String questiontext, String answertext, String categories) throws IOException, SQLException {
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 if (questiontype.equals("QR")){
			 storeQuestionStmt = con.prepareStatement("insert into questionResponse values (null, ?, ?, ?, ?)");
		 }
		 if (questiontype.equals("FB")){
			 storeQuestionStmt = con.prepareStatement("insert into fillInTheBlank values (null, ?, ?, ?, ?)");
		 }
		 if (questiontype.equals("MC")){
			 storeQuestionStmt = con.prepareStatement("insert into multipleChoice values (null, ?, ?, ?, ?)");
		 }
		 if (questiontype.equals("PR")){
			 storeQuestionStmt = con.prepareStatement("insert into pictureResponse values (null, ?, ?, ?, ?)");
		 }
		 int i = 1;
		 storeQuestionStmt.setString(i++, categories);
		 storeQuestionStmt.setString(i++, questiontext);
		 storeQuestionStmt.setString(i++, answertext);
		 java.sql.Date now = new java.sql.Date(System.currentTimeMillis());
		 storeQuestionStmt.setDate(i++, now);
		 storeQuestionStmt.executeUpdate();

		 stmt.close();
		 storeQuestionStmt.close();
		 con.close();
		 
		 int maxquestionid = maxquery(questiontype);	 
		 String questionID = questiontype + Integer.toString(maxquestionid);
		 
		 return questionID;
	 }

	 public static int maxquery(String questiontype) throws IOException, SQLException {
		 int max = 0;
		 try {
			 Class.forName("com.mysql.jdbc.Driver");
			 java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
			 Statement stmt = con.createStatement();
			 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			 String sqlquery = null;
			 
			 if (questiontype.equals("QR")){
				 sqlquery = "SELECT MAX(questionId) FROM questionResponse";
			 }
			 if (questiontype.equals("FB")){
				 sqlquery = "SELECT MAX(questionId) FROM fillInTheBlank";
			 }
			 if (questiontype.equals("MC")){
				 sqlquery = "SELECT MAX(questionId) FROM multipleChoice";
			 }
			 if (questiontype.equals("PR")){
				 sqlquery = "SELECT MAX(questionId) FROM pictureResponse";
			 }
			 
			 ResultSet rs = stmt.executeQuery(sqlquery);	
			 while (rs.next()){
			 	max = rs.getInt(1);
			 }
			 con.close();
			 }catch (SQLException e) {
			 	e.printStackTrace();
			 } catch (ClassNotFoundException e) {
			 	e.printStackTrace();
			 }
		return max;	
	 }	 

	 public static int getLastQuizID() throws IOException, SQLException {
		 int lastID = 0;
		 try {
			 Class.forName("com.mysql.jdbc.Driver");
			 java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
			 Statement stmt = con.createStatement();
			 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			 String sqlquery = null;
			 
			 sqlquery = "SELECT MAX(quizId) FROM quizzes";
			 
			 ResultSet rs = stmt.executeQuery(sqlquery);	
			 while (rs.next()){
				 lastID = rs.getInt(1);
			 }
			 con.close();
			 }catch (SQLException e) {
			 	e.printStackTrace();
			 } catch (ClassNotFoundException e) {
			 	e.printStackTrace();
			 }
		return lastID;	
	 }		 
	 
	 public static ArrayList<String> getQuestion(String id) throws IOException, SQLException {
		 ArrayList<String> questionarray = new ArrayList<String>();
		 String questiontype = id.substring(0, 2);
		 int questionid = Integer.parseInt(id.substring(2));
		 try {
			 Class.forName("com.mysql.jdbc.Driver");
			 java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
			 Statement stmt = con.createStatement();
			 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			 String sqlquery = null;
			 
			 if (questiontype.equals("QR")){
				 sqlquery = "SELECT * FROM questionResponse WHERE questionId = "+ questionid;
			 }
			 if (questiontype.equals("FB")){
				 sqlquery = "SELECT * FROM fillInTheBlank WHERE questionId = "+ questionid;
			 }
			 if (questiontype.equals("MC")){
				 sqlquery = "SELECT * FROM multipleChoice WHERE questionId = "+ questionid;
			 }
			 if (questiontype.equals("PR")){
				 sqlquery = "SELECT * FROM pictureResponseWHERE questionId = "+ questionid;
			 }
			 			 
			 ResultSet rs = stmt.executeQuery(sqlquery);	
			 while (rs.next()){
			 	questionarray.add(rs.getString("questionString"));
			 	questionarray.add(rs.getString("answerString"));
			 }
			 con.close();
			 }catch (SQLException e) {
			 	e.printStackTrace();
			 } catch (ClassNotFoundException e) {
			 	e.printStackTrace();
			 }
		return questionarray;	
	 }	 

	 public synchronized static void updateHighScore (int quizid, int userid, int score) throws IOException, SQLException {
		 updateTopScore(quizid, score);
		 updateTopScorer(quizid, userid);
	 }
	 
	 public synchronized static void updateTopScore (int quizid, int score) throws IOException, SQLException {	 
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 updateHighScoreStmt  = con.prepareStatement("update quizzes SET topScore = ? where quizId = ?");

		 int i = 1;
		 updateHighScoreStmt.setInt(i++, score);
		 updateHighScoreStmt.setInt(i++, quizid);
		 updateHighScoreStmt.executeUpdate();
		 
		 stmt.close();
		 con.close();
		 updateHighScoreStmt.close();
	 }
	 
	 public synchronized static void updateTopScorer (int quizid, int userid) throws IOException, SQLException {	 
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 updateHighScorerStmt  = con.prepareStatement("update quizzes SET topScoreUserId = ? where quizId = ?");

		 int i = 1;
		 updateHighScorerStmt.setInt(i++, userid);
		 updateHighScorerStmt.setInt(i++, quizid);
		 updateHighScorerStmt.executeUpdate();
		 
		 stmt.close();
		 con.close();
		 updateHighScorerStmt.close();
	 }

	 public synchronized static void updateBestTime (int quizid, int userid, int time) throws IOException, SQLException {
		 updateTopTime(quizid, time);
		 updateTopTimeUser(quizid, userid);
	 }
	 
	 public synchronized static void updateTopTime (int quizid, int time) throws IOException, SQLException {	 
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 updateBestTimeStmt  = con.prepareStatement("update quizzes SET topTime = ? where quizId = ?");

		 int i = 1;
		 updateBestTimeStmt.setInt(i++, time);
		 updateBestTimeStmt.setInt(i++, quizid);
		 updateBestTimeStmt.executeUpdate();
		 
		 stmt.close();
		 con.close();
		 updateBestTimeStmt.close();
	 }
	 
	 public synchronized static void updateTopTimeUser (int quizid, int userid) throws IOException, SQLException {	 
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 updateBestTimeUserStmt  = con.prepareStatement("update quizzes SET topTimeUserId = ? where quizId = ?");

		 int i = 1;
		 updateBestTimeUserStmt.setInt(i++, userid);
		 updateBestTimeUserStmt.setInt(i++, quizid);
		 updateBestTimeUserStmt.executeUpdate();
		 
		 stmt.close();
		 con.close();
		 updateBestTimeUserStmt.close();
	 }
	 
	public static String arrayToString (ArrayList<String> array){
		String result = "";
		for (int i = 0; i<array.size(); i++){
			result = result + array.get(i);
			if (i != array.size()-1){
				result = result + ",";
			}
		}	
		return result;
	}
	 
	public synchronized static void submitQuiz(Quiz myquiz) throws IOException, SQLException {
		Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		Statement stmt = con.createStatement();
		stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		// Set up the PreparedStatements 
		submitQuizStmt = con.prepareStatement("insert into quizzes values (null, ?, ?, 0, null, null, null, ?, ?, ?, ?, ?)");

		int i = 1;
		java.sql.Date now = new java.sql.Date(System.currentTimeMillis());
		submitQuizStmt.setDate(i++, now);
		submitQuizStmt.setString(i++, arrayToString(myquiz.getCategories()));
		submitQuizStmt.setInt(i++,  myquiz.getRating());
		submitQuizStmt.setString(i++, myquiz.getName());
		submitQuizStmt.setInt(i++, myquiz.getRandomInt());
		submitQuizStmt.setInt(i++, myquiz.getPracticeInt());
		submitQuizStmt.setInt(i++, myquiz.getSinglePageInt());
		submitQuizStmt.executeUpdate();
		
		int insertedQuizID = getLastQuizID();
		ArrayList<Question> myQuestions = myquiz.getQuestions();
		addToQuestionQuizLink(insertedQuizID, myQuestions);
	}

	 public synchronized static void addToQuestionQuizLink (int quizid, ArrayList<Question> myquestions) throws IOException, SQLException {
		 Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
		 Statement stmt = con.createStatement();
		 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
		 
		 // Set up the PreparedStatements 
		 addQuizQuestionLinkStmt = con.prepareStatement("insert into questionQuizLinkage values (?, ?, ?)");

		 for (int j=0; j<myquestions.size(); j++){
			 int i = 1;
			 String questiontype = myquestions.get(j).getQuestionId().substring(0, 2);
			 int questionid = Integer.parseInt(myquestions.get(j).getQuestionId().substring(2));
			 addQuizQuestionLinkStmt.setInt(i++, questionid);
			 addQuizQuestionLinkStmt.setString(i++, questiontype);
			 addQuizQuestionLinkStmt.setInt(i++, quizid);
			 addQuizQuestionLinkStmt.executeUpdate();
		 }
		 stmt.close();
		 addQuizQuestionLinkStmt.close();
		 con.close();
	 } 	
	 
	public static ArrayList<String> stringToArray (String commaseparatedstring){
		ArrayList<String> tagarray = new ArrayList<String>();
		String[] array = commaseparatedstring.split(",");
		for (int i = 0; i<array.length; i++){
			tagarray.add(array[i]);
		}
		return tagarray;
	}
	
	public static int boolToInt(boolean bool) {
		if(bool == false) {
			return 0;
		} else {
			return 1;
		}
	}
	
	public static boolean intToBool(int num) {
		if(num == 0) {
			return false;
		} else {
			return true;
		}
	}

	 public synchronized static Quiz getQuiz (int quizid) {
		 Quiz newQuiz = new Quiz();
		 try {
			 Class.forName("com.mysql.jdbc.Driver");
			 java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
			 Statement stmt = con.createStatement();
			 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			 String sqlquery = "SELECT * FROM quizzes WHERE quizId = '"+ quizid + "'";
			 ResultSet rs = stmt.executeQuery(sqlquery);	
			 while (rs.next()){
				 newQuiz.setCreatedDate(rs.getDate("quizCreatedDate"));
				 newQuiz.setCategories(stringToArray(rs.getString("quizCategories")));;
				 newQuiz.setTopScore(rs.getInt("topScore"));
				 newQuiz.setTopUser(rs.getInt("topScoreUserId"));
				 newQuiz.setTopTime(rs.getInt("topTime"));
				 newQuiz.setTopTimeUser(rs.getInt("topTimeUserId"));
				 newQuiz.setRating(rs.getInt("rating"));
				 newQuiz.setName(rs.getString("quizName"));
				 newQuiz.setRandom(intToBool(rs.getInt("isRandom")));
				 newQuiz.setPractice(intToBool(rs.getInt("allowsPractice")));
				 newQuiz.setSinglePage(intToBool(rs.getInt("isSinglePage")));
				 newQuiz.setId(quizid);
			 }
			 con.close();
		 		}catch (SQLException e) {
		 			e.printStackTrace();
		 		} catch (ClassNotFoundException e) {
				 	e.printStackTrace();
		 		}
		 	newQuiz.setQuestions(getQuizQuestions(quizid));;
			return newQuiz;	
	 }
	
	 public static ArrayList<Question> getQuizQuestions(int quizid) {
		 ArrayList<Question> myQuestions = new ArrayList<Question>();
		 try { 
			 Class.forName("com.mysql.jdbc.Driver");
			 java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://" + MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME ,MyDBInfo.MYSQL_PASSWORD);
			 Statement stmt = con.createStatement();
			 stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			 String sqlquery = "SELECT * FROM questionQuizLinkage WHERE quizId = '"+ quizid + "'";
			 ResultSet rs = stmt.executeQuery(sqlquery);	
			 while(rs.next()) {
				 String questionId = rs.getString("questionId");
				 myQuestions.add(new Question(questionId));
			 }
			 con.close();
	 		}catch (SQLException e) {
	 			e.printStackTrace();
	 		} catch (ClassNotFoundException e) {
			 	e.printStackTrace();
	 		}
		 return myQuestions;
	 }

	
}
