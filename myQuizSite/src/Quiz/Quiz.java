package Quiz;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspWriter;
import javax.servlet.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.sql.*;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import database.DBConnection;

public class Quiz {
	
	private ArrayList<Question> questions;
	private boolean isRandom;
	private boolean allowsPractice;
	private boolean isSinglePage;
	private int topScore;
	private int topUserId;
	private int topTimeUserId;
	private int rating;
	private int topTime;
	private String name;
	private ArrayList<String> categories;
	private Date created;
	private int id;
	
	@SuppressWarnings("unchecked")
	private void getQuiz(Quiz myQuiz, int id, Connection connect) {
		try {	
			ResultSet rs;
    		Statement stmt = connect.createStatement();
    		String query = "SELECT * FROM quizzes where quizId = \""+ 
    			Integer.toString(id) + "\"";
    		rs = stmt.executeQuery(query);
    		rs.next();
    		
    		myQuiz.setTopScore(rs.getInt("topScore"));
    		myQuiz.setTopUser(rs.getInt("topUserID"));
    		myQuiz.setRating(rs.getInt("rating"));
    		myQuiz.setTopTime(rs.getInt("topTime"));
    		Date myDate = rs.getTimestamp("quizCreatedDate");
    		myQuiz.setCreatedDate(myDate);
    		myQuiz.setCreatedDate(rs.getDate("quizCreatedDate"));
    		myQuiz.setRandom(intToBool(rs.getInt("isRandom")));
    		myQuiz.setPractice(intToBool(rs.getInt("allowsPractice")));
    		myQuiz.setSinglePage(intToBool(rs.getInt("isSinglePage")));
    		myQuiz.setName(rs.getString("quizName"));
    		myQuiz.setCategories((ArrayList<String>)rs.getObject("quizCategories"));
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}

    	// Get questions for quiz
		ArrayList<Question> myQuestions = new ArrayList<Question>();
    	try {
			ResultSet rs;
    		Statement stmt = connect.createStatement();
	    	String questionQuery = "SELECT * FROM questionQuizLinkage where quizID='" + Integer.toString(id)+"'";
    		rs = stmt.executeQuery(questionQuery);
    		while(rs.next()) {
    			String questionId = rs.getString("questionId");
    			myQuestions.add(new Question(questionId));
    		}
		} catch (SQLException e) {
			e.printStackTrace();
    	}
		myQuiz.setQuestions(myQuestions);
	}
	
	public Quiz() {
		topScore = -1;
		topUserId = -1;
		rating = -1;
		topTime = -1;
		id = -1;
	}
	
	// Adds this quiz to the database, returns quiz ID
	public void submitQuiz(Connection connect) {
		int quizId = -1;
		try {	
    		Statement stmt = connect.createStatement();
    		java.sql.Date sqlDate = new java.sql.Date(this.getCreatedDate().getTime());
    		String newLine = "INSERT INTO quizzes " +
    				"(quizCreatedDate, topScore, topTime, topUserID, rating, quizName, " +
    				"isRandom, allowsPractice, isSinglePage) " +
    				"VALUES ('" + sqlDate + "', '"+this.getTopScore()+"', '"+ this.getTopTime() + "', '"+ this.getTopUser() +"', '"+ this.getRating() +
    				"', '"+ this.getName() +"', '"+ this.getRandomInt() +"', '" + this.getPracticeInt() +
    				"', '"+ this.getSinglePageInt() + "')";
    		System.out.println(newLine);
    		stmt.executeUpdate(newLine);
    		Statement selectStmt = connect.createStatement();
    		String getId = "select LAST_INSERT_ID()";
    		ResultSet rs = stmt.executeQuery(getId);
    		rs.next();
    		quizId = rs.getInt("last_insert_id()");
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
    	
    	try {
    		Statement stmt = connect.createStatement();
    		String newLine = "";
    		
    	} catch(SQLException e) {
    		e.printStackTrace();
    	}
    	
    	ArrayList<Question> myQuestions = this.getQuestions();
    	try {
    		for(int i = 0; i < myQuestions.size(); i++) {
    			// Parse question
    			Question myQ = myQuestions.get(i);
    			String questionType = myQ.getQuestionId().substring(0, 2);
    			int questionId = Integer.parseInt(myQ.getQuestionId().substring(2));
    			// Insert question into database
        		Statement stmt = connect.createStatement();
        		String newLine = "INSERT INTO questionQuizLinkage " + "(questionId, questionType, quizId)"
        			+ " VALUES ('"+ questionId +"','"+ questionType +"','"+ quizId +"')";
        		System.out.println(newLine);
        		stmt.executeUpdate(newLine);
    		}
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
    	
	}
	
	public void showSummaryPage(JspWriter out) {
		try {
			out.println("Top Score: " + topScore);
			out.println("Fastest Time: " + topTime);
			out.println("High Score User: " + topUserId);
			out.println("Rating: " + rating);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<Question> getQuestions() {
		return questions;
	}
	
	public void addQuestion(Question q) {
		questions.add(q);
	}
	
	public void setName(String myName) {
		name = myName;
	}
	
	public String getName() {
		return name;
	}
	
	public Date getCreatedDate() {
		return created;
	}
	
	public void setCreatedDate(Date currentDate) {
		created = currentDate;
	}
	
	public void setRandom(boolean setting) {
		isRandom = setting;
	}
	
	public void setPractice(boolean setting) {
		allowsPractice = setting;
	}
	
	public void setSinglePage(boolean setting) {
		isSinglePage = setting;
	}
	
	public void setQuestions(ArrayList<Question> createdQuestions) {
		questions = createdQuestions;
	}
	
	public void administer(JspWriter out) {
		// Administer this quiz
	}
	
	public int numberOfQuestions(){
		return questions.size();
	}
	
	/**
	 * 
	 * @return Highest score gotten on quiz
	 */
	public int getTopScore() {
		return topScore;
	}
	
	public void setTopScore(int score) {
		topScore = score;
	}
	
	/**
	 * 
	 * @return Fastest time for completing quiz
	 */
	public int getTopTime() {
		return topTime;
	}
	
	public void setTopTime(int time) {
		topTime = time;
	}
	
	/**
	 * 
	 * @return ID of user with top score
	 */
	public int getTopUser() {
		return topUserId;
	}
	
	public void setTopUser(int userId) {
		topUserId = userId;
	}
	
	public void setTopTimeUser(int userId) {
		topTimeUserId = userId;
	}
	
	public int getTopTimeUser(int userId) {
		return topTimeUserId;
	}
	
	/**
	 * 
	 * @return Rating based on feedback of users
	 * taking the quiz
	 */
	public int getRating() {
		return rating;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int myId) {
		id = myId;
	}
	
	public ArrayList<String> getCategories() {
		return categories;
	}
	
	public void setCategories(ArrayList<String> myCategories) {
		categories = myCategories;
	}
	
	public void setRating(int inputRating) {
		rating = inputRating;
	}
	
	public boolean getRandom() {
		return isRandom;
	}
	
	public boolean getSinglePage() {
		return isSinglePage;
	}
	
	public boolean getPractice() {
		return allowsPractice;
	}
	
	public int getRandomInt() {
		return boolToInt(isRandom);
	}
	
	public int getSinglePageInt() {
		return boolToInt(isSinglePage);
	}
	
	public int getPracticeInt() {
		return boolToInt(allowsPractice);
	}
	
	private int boolToInt(boolean bool) {
		if(bool == false) {
			return 0;
		} else {
			return 1;
		}
	}
	
	private boolean intToBool(int num) {
		if(num == 0) {
			return false;
		} else {
			return true;
		}
	}
	
}
