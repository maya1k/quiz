package Quiz;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * Servlet implementation class TakeQuiz
 */
@WebServlet("/TakeQuiz")
public class TakeQuiz extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TakeQuiz() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sess = request.getSession();
		ArrayList<Question> CurrentQuestions = (ArrayList<Question>)sess.getAttribute("CurrentQuestions");
		ArrayList<String> UnaskedQuestionIds = (ArrayList<String>)sess.getAttribute("UnaskedQuestionIds");
		Quiz myQuiz = (Quiz)sess.getAttribute("quiz");

		int score = (Integer)sess.getAttribute("score");
		
		if(UnaskedQuestionIds.size() == 0) {
			sess.setAttribute("score", score);
			RequestDispatcher dispatch = request.getRequestDispatcher("FinishQuiz");
			dispatch.forward(request, response);	
		} else {
			if(myQuiz.getSinglePage()) {
				RequestDispatcher dispatch = request.getRequestDispatcher("QuestionDisplay.jsp");
				dispatch.forward(request, response);
			} else {
				// Add option to randomize
				String nextQuestionId;
				if(myQuiz.getRandom()) {
					Random rand = new Random();
					int nextQuestionIndex =  rand.nextInt(UnaskedQuestionIds.size());
					nextQuestionId = UnaskedQuestionIds.get(nextQuestionIndex);
					UnaskedQuestionIds.remove(nextQuestionIndex);
				} else {
					nextQuestionId = UnaskedQuestionIds.get(0);
					UnaskedQuestionIds.remove(0);
				}
				
				// Selection question to ask
				boolean set = false;
				for(int i = 0; i < CurrentQuestions.size(); i++) {
					if(CurrentQuestions.get(i).getQuestionId().equals(nextQuestionId)) {
						sess.setAttribute("AskQuestion", CurrentQuestions.get(i));
						set = true;
					}
				}
				if(!set) {
					System.out.println("Didn't find question!");
					RequestDispatcher dispatch = request.getRequestDispatcher("Error.jsp");
					dispatch.forward(request, response);	
				}
							
				System.out.println("Playing question with id " + nextQuestionId);
				RequestDispatcher dispatch = request.getRequestDispatcher("QuestionDisplay.jsp");
				dispatch.forward(request, response);
			}
		}
	}

}
