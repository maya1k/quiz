package Quiz;

public class Question {
	
	private String id;
	
	public Question(String myId) {
		id = myId;
	}
	
	public String getQuestionId() {
		return id;
	}
	
	public String getYourAnswer() {
		return "youranswer";
	}
	
	public String getCorrectAnswer() {
		return "correctanswer";
	}
	
	public int getScore() {
		return 1;
	}
}
