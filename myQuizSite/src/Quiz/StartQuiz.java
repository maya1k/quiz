package Quiz;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import javax.servlet.http.HttpSession;
import javax.servlet.*;
import java.sql.*;

import database.DBConnection;

/**
 * Servlet implementation class StartQuiz
 */
@WebServlet("/StartQuiz")
public class StartQuiz extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StartQuiz() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletContext context = getServletContext();
		HttpSession sess = request.getSession();
		Quiz myQuiz = (Quiz)sess.getAttribute("quiz");
		ArrayList<Question> questions = myQuiz.getQuestions();
		ArrayList<String> unaskedQuestionIds = new ArrayList<String>();
		int startTime = (int)System.currentTimeMillis();
		
		// Make list of unasked question ID's
		for(int i = 0; i < questions.size(); i++) {
			String questionId = questions.get(i).getQuestionId();
			unaskedQuestionIds.add(questionId);
		}
							
		sess.setAttribute("CurrentQuestions", questions);
		sess.setAttribute("UnaskedQuestionIds", unaskedQuestionIds);
		sess.setAttribute("score", 0);
		sess.setAttribute("startTime", startTime);
		
		RequestDispatcher dispatch = request.getRequestDispatcher("TakeQuiz");
		dispatch.forward(request, response);			
	}

}
