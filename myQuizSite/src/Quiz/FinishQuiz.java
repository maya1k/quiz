package Quiz;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import database.DBConnection;

import java.sql.SQLException;
import java.util.*;

/**
 * Servlet implementation class FinishQuiz
 */
@WebServlet("/FinishQuiz")
public class FinishQuiz extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FinishQuiz() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Check to see if highest score has been exceeded
		// Set the user score
		HttpSession sess = request.getSession();
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		int score = (Integer)sess.getAttribute("score");
		int elapsed = (int)System.currentTimeMillis() - (Integer)sess.getAttribute("startTime");
		Quiz quiz = (Quiz)sess.getAttribute("quiz");
		int quizId = quiz.getId();
		int userId = 1; // update to get out of session
		
		// Update history in database
		try {
			DBConnection.addHistory(userId, score, elapsed, quizId);
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head>");
		out.println("<meta charset=\"UTF-8\" />");
		out.println("<title>Quiz finished</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>Score: " + sess.getAttribute("score") + "</h1>");
		if(score > quiz.getTopScore() || quiz.getTopScore() == -1) {
			try {
				DBConnection.updateHighScore(quizId, userId, score);
				out.println("<h1><b>HIGH SCORE!<b></h1>");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		out.println("Time: " + elapsed);
		if(elapsed < quiz.getTopTime() || quiz.getTopTime() == -1) {
			try {
				DBConnection.updateBestTime(quizId, userId, elapsed);
				out.println("<h1><b>BEST TIME!</b></h1>");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		out.println("<form action=\"index.jsp\"><input type=\"submit\" value=\"Return home\"></form>");
		out.println("</body>");
		out.println("</html>");
		
		sess.setAttribute("score", null);
		sess.setAttribute("CurrentQuestions", null);
		sess.setAttribute("UnaskedQuestionIds", null);
		sess.setAttribute("AskQuestion", null);
	}

}
