package Quiz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class QuizCreation
 */
@WebServlet("/QuizCreation")
public class QuizCreation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QuizCreation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean isRandom = (request.getParameter("isRandom").equals("yes"));
		System.out.println(request.getParameter("isRandom"));
		boolean allowsPractice = (request.getParameter("allowsPractice").equals("yes"));
		boolean isSinglePage = (request.getParameter("isSinglePage").equals("yes"));
		String name = request.getParameter("name");
		String catString = request.getParameter("categories");
		ArrayList<String> categories = new ArrayList();
		StringTokenizer tok = new StringTokenizer(catString, ",");
		while(tok.hasMoreTokens()) {
			categories.add(tok.nextToken());
		}
		
		ArrayList<Question> questions = new ArrayList<Question>();

		Quiz myQuiz = new Quiz();
		myQuiz.setRandom(isRandom);
		myQuiz.setPractice(allowsPractice);
		myQuiz.setSinglePage(isSinglePage);
		myQuiz.setQuestions(questions);
		myQuiz.setName(name);
		myQuiz.setCreatedDate(new Date());
		myQuiz.setCategories(categories);
		
		HttpSession sess = request.getSession();
		sess.setAttribute("currentlyCreating", name);
		sess.setAttribute("quiz", myQuiz);
		sess.setAttribute("CreatedQuestion", null);
		RequestDispatcher dispatch = request.getRequestDispatcher("NextQuestionOption.jsp");
		dispatch.forward(request, response);	
	}

}
