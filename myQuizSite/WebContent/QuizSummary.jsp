<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*, Quiz.Quiz, database.DBConnection" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Quiz Overview</title>
</head>
<body>
<%
String quizId = (String)request.getParameter("quizId");
HttpSession sess = request.getSession();
int id = Integer.parseInt(quizId);
Quiz myQuiz = DBConnection.getQuiz(id);
System.out.println(myQuiz);
if(myQuiz == null) {
	RequestDispatcher rd = request.getRequestDispatcher("error.jsp");
	rd.forward(request, response);
}
sess.setAttribute("quizId", quizId);
sess.setAttribute("quiz", myQuiz);

out.println("Name: " + myQuiz.getName());
out.println("<br>");
out.println("Description: sample description");
out.println("<br>");
out.println("Categories: " );
out.println("<ul>");
for(int i = 0; i < + myQuiz.getCategories().size(); i++) {
	out.println("<li>" + myQuiz.getCategories().get(i) + "</li>");
}
out.println("</ul>");
if(myQuiz.getPractice()) {
	sess.setAttribute("practice", true);
	out.println("<form action =\"StartQuiz\" method =\"post\">");
	out.println("<input type=\"submit\" value=\"Pratice me\">");
	out.println("</form>");
} else {
	sess.setAttribute("practice", false);
}
%>
<br>
<form action ="StartQuiz" method ="post">
<input type="submit" value="Take Quiz">
</form>
<br>
<form action="index.jsp">
<input type="submit" value="Return home">
</form>
</body>
</html>