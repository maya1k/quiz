<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*, Quiz.Question, Quiz.Quiz" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
HttpSession sess = request.getSession();
Quiz myQuiz = (Quiz)sess.getAttribute("quiz");
int myScore = (Integer)sess.getAttribute("score");
if(myQuiz.getSinglePage()) {
	ArrayList<Question> questions = (ArrayList<Question>)sess.getAttribute("CurrentQuestions");
	for(int i = 0; i < questions.size(); i++) {
		myScore += questions.get(i).getScore();
	}
	sess.setAttribute("score", myScore);
	RequestDispatcher dispatch = request.getRequestDispatcher("FinishQuiz");
	dispatch.forward(request, response);
} else {
	Question myQuestion = (Question)sess.getAttribute("AskQuestion");
	myScore += myQuestion.getScore();
	sess.setAttribute("score", myScore);
	out.println("You answered: " + myQuestion.getYourAnswer() + "<br>");
	out.println("The correct answer is: " + myQuestion.getCorrectAnswer() + "<br>");
	out.println("You earned: " + myQuestion.getScore() + "<br>");
	out.println("Your score is: " + myScore + "<br>");
	out.println("<form action=\"TakeQuiz\" method=\"post\">");
	out.println("<input type=\"Submit\" value=\"Keep Playing\">");
	out.println("</form>");
}
%>
</body>
</html>