<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*, Quiz.Question, Quiz.Quiz" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Answer the question</title>
</head>
<body>
<form method="post" action="QuestionResult.jsp">
<%
HttpSession sess = request.getSession();
Quiz myQuiz = (Quiz)sess.getAttribute("quiz");
if(myQuiz.getSinglePage()) {
	ArrayList<Question> questions = (ArrayList<Question>)sess.getAttribute("CurrentQuestions");
	for(Question q : questions){
		out.println("Displaying question with id " + q.getQuestionId());
		out.println("<br>");
		//q.display(out);
	}
} else {
	Question currentQuestion = (Question)sess.getAttribute("AskQuestion");
	out.println("Currently taking question with ID" + currentQuestion.getQuestionId());
}

%>
<input type="submit" value="Grade me!">
</form>
</body>
</html>