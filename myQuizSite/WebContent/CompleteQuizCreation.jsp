<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*, Quiz.Quiz, java.sql.*, database.DBConnection" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Quiz Complete</title>
</head>
<body>
You have finished your quiz!
<br>
Number of questions: 
<% 
Quiz myQuiz = (Quiz)session.getAttribute("quiz");
out.println(myQuiz.numberOfQuestions());
ServletContext context = session.getServletContext();
try {
	DBConnection.submitQuiz(myQuiz);
} catch(SQLException e) {
	e.printStackTrace();
}
session.setAttribute("currentlyCreating", null); 
%>
<br>
<form action="index.jsp"><input type="submit" value="Return home"></form>
</body>
</html>