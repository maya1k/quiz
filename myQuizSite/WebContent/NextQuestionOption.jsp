<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*, Quiz.Quiz, Quiz.Question" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Do you want to add a question?</title>
</head>
<body>
<%
/*if(session.getAttribute("quiz").equals(null)) {
	RequestDispatcher dispatch = request.getRequestDispatcher("error.jsp");
	dispatch.forward(request, response);
} else */
if(session.getAttribute("CreatedQuestion") != null) {
	if(session.getAttribute("CreatedQuestion").equals("0")) {
		RequestDispatcher dispatch = request.getRequestDispatcher("error.jsp");
		dispatch.forward(request, response);		
	} else {
		//System.out.println((String)session.getAttribute("currentlyCreating") + " : " + (String)session.getAttribute("CreatedQuestion"));
		Quiz myQuiz = (Quiz)session.getAttribute("quiz");
		Question newQuestion = (Question)session.getAttribute("CreatedQuestion");
		myQuiz.addQuestion(newQuestion);
		session.setAttribute("CreatedQuestion", null);
	}	
}

%>
<form action ="MakeQuestion.jsp">
<input type="submit" value="Make question">
</form>
<form action ="CompleteQuizCreation.jsp">
<input type="submit" value="Finish quiz">
</form>
</body>
</html>