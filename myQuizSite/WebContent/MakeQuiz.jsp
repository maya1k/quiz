<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Make a new quiz</title>
</head>
<body>
<form action="QuizCreation" method="post">
<input type="text" name ="name" value="Enter name here">
<br>
<input type="text" name="categories" value="Enter categories separated by ,">
<br>
<b>Quiz is random.</b>
<input type ="radio" name="isRandom" value="yes">Yes
<input type ="radio" name="isRandom" value="no" checked>No
<br>
<b>Quiz allows practice.</b>
<input type ="radio" name="allowsPractice" value="yes">Yes
<input type ="radio" name="allowsPractice" value="no" checked>No 
<br>
<b>Quiz is single-page.</b>
<input type ="radio" name="isSinglePage" value="yes">Yes
<input type ="radio" name="isSinglePage" value="no" checked>No
<br>
<input type="submit" name="Submit">
</form>
</body>
</html>