<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*, Quiz.Question" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New question</title>
</head>
<body>
This is the dummy page that sets the ID to some random number
<% 
Random rand = new Random();
int nextInt =  rand.nextInt(100);
String nextQuestionId = "MC" + Integer.toString(nextInt);
Question nextQuestion = new Question(nextQuestionId);
session.setAttribute("CreatedQuestionId", nextQuestionId); 
session.setAttribute("CreatedQuestion", nextQuestion); 
%>
<form action="NextQuestionOption.jsp">
<input type="submit" value="Make question">
</form>

</body>
</html>